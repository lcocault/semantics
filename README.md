# Welcome

Welcome to the Semantics project, a set of Java modules parse and process texts
in natural languages.

The first module provided in the current version is the [dictionary module](dictionary/README.md). It supports French dictionary but is designed to support many other languages.

The second module is the [completion module](completion/README.md). It is inspired by a article of GNU Linux Managzine France of May 2017. Basic concepts have been implemented in Java and extended to be more efficient.

# License

All modules are licensed by Laurent COCAULT under the Apache License Version 2.0.
A copy of this license is provided in the LICENSE.txt file of each module.

