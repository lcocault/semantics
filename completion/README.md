# Scope

The Java based completion library is a set of mechanisms that provide
completion feature for natural languages.

#License

It is licensed by Laurent COCAULT under the Apache License Version 2.0. A copy
of this license is provided in the LICENSE.txt file.

# Content

The src/main/java directory contains the library sources.
The src/main/resources directory contains the library data.
The src/test/java directory contains the tests sources.
The src/test/resources directory contains the tests data.

# Dependencies

Java based completion relies on the following free software, all released under
business friendly free licenses.

test-time dependency:

  - JUnit 4.12 from Erich Gamma and Kent Beck
    http://www.junit.org/
    released under the Common Public License Version 1.0
