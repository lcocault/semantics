/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static org.junit.Assert.assertEquals;

public class TestCorpusParser {

    @Test
    public void testBasic() {

        // Fixture
        String text = "Hello Helen! He is \nhere. He hears me, if \nhe " +
            "is\n  near.";
        ByteArrayInputStream bis = new ByteArrayInputStream(text.getBytes());

        // Tested object
        CorpusParser parser = new CorpusParser();
        parser.parse(bis);
        assertEquals(3, parser.getRoot().getSubNodeCount("he"));
        System.out.println(parser.toString());
    }

}
