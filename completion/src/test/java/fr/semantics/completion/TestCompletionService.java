/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;

import org.junit.Test;

public class TestCompletionService {

    @Test
    public void testBasic() {

        // Fixture
        CorpusParser parser = createFixture();

        // Tested object
        CompletionService completion = new CompletionService(parser.getRoot());
        assertEquals("he", completion.resolve(new String[0], ""));
        assertEquals("he", completion.resolve(new String[0], "H"));
        assertEquals("hello", completion.resolve(new String[0], "hel"));
        assertEquals("helen", completion.resolve(new String[] {"Hello"}, "h"));
        assertEquals("here",
                completion.resolve(new String[] {"He", "is"}, "h"));
        assertEquals("he", completion.resolve(new String[] {"No", "way"}, "h"));

    }

    /**
     * Fixture of the test.
     * @return Fixture parser
     */
    private CorpusParser createFixture() {
        // Create a parser as the fixture of the test
        String text = "Hello Helen! He is \nhere. He hears me, if \nhe " +
                "is\n  near.";
        ByteArrayInputStream bis = new ByteArrayInputStream(text.getBytes());
        CorpusParser parser = new CorpusParser();
        parser.parse(bis);
        return parser;
    }

}
