/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;

public class TestNode {

    @Test
    public void testBasic() {

        // Tested object
        Node node = new Node("Root");

        // Check word
        assertEquals("Root", node.getWord());

        // Add sub-nodes
        node.addSubNode("Sub1");
        node.addSubNode("Sub2");
        node.addSubNode("Sub1");

        // Check subnodes
        assertEquals(1, node.getSubNodeCount("Sub2"));
        assertEquals(2, node.getSubNodeCount("Sub1"));
        assertEquals(0, node.getSubNodeCount("Sub3"));

        // Clear the node
        node.clear();
        assertEquals(0, node.getSubNodeCount("Sub1"));
    }

}
