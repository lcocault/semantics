/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Completion node.
 */
public class Node implements Serializable {

    /**
     * Word associated to the node.
     */
    private String word;

    /**
     * Transitions to sub-nodes with associated counts.
     */
    private Map<Node, Integer> subNodes;

    /**
     * Constructor.
     *
     * @param word Word associated with the node
     */
    public Node(final String word) {
        this.word = word;
        subNodes = new HashMap<>();
    }

    /**
     * Add a sub-node to the node. If the node already exists, the associated
     * count is incremented. Otherwise, the node is created with an initial
     * count of 1.
     *
     * @param subNode Sub-node to add
     * @return Added sub-node
     */
    public Node addSubNode(final String subNode) {
        // Search for an existing node
        Node node = searchNode(subNode);
        if (node == null) {
            // No existing node found. Create the new node with a count
            // initialized to 1 and return the node created.
            node = new Node(subNode);
            subNodes.put(node, 1);
        } else {
            // Node found, increment
            final Integer count = subNodes.get(node);
            subNodes.put(node, count + 1);
        }
        return node;
    }

    /**
     * Clear the node.
     */
    public void clear() {
        subNodes.clear();
    }

    /**
     * Get the count associated with a sub-node.
     *
     * @param subNodeWord Word of the sub-node
     * @return Count associated with the sub-node, 0 if the sub-node does not
     * exist
     */
    public int getSubNodeCount(final String subNodeWord) {
        final Node subNode = searchNode(subNodeWord);
        if (subNode == null) {
            return 0;
        } else {
            return subNodes.get(subNode);
        }
    }

    /**
     * Get the word.
     *
     * @return Word value
     */
    public String getWord() {
        return word;
    }


    /**
     * Get the words and associated scores.
     *
     * @return Map of scores indexed by nodes
     */
    protected Map<Node, Integer> getSubNodes() {
        return subNodes;
    }

    /**
     * Search for an existing sub-node.
     *
     * @param searchedWord Word of the sub-node to search
     * @return Found node, or null if the node does not exist
     */
    protected Node searchNode(final String searchedWord) {
        for (Node node : subNodes.keySet()) {
            if (searchedWord.equals(node.getWord())) {
                // Return the node
                return node;
            }
        }
        // The node is not found
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        final StringBuffer stringified = new StringBuffer(word + "[");
        for (Node subNode : subNodes.keySet()) {
            stringified.append(subNode + "," + subNodes.get(subNode) + " ");
        }
        stringified.append("]");
        return stringified.toString();
    }

}
