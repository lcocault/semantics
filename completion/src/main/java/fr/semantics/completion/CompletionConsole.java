/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Simple GUI to illustrate the completion service.
 */
public class CompletionConsole extends Application {

    /**
     * Default height of the window in pixels.
     */
    private static final int DEFAULT_HEIGHT = 200;

    /**
     * Default width of the window in pixels.
     */
    private static final int DEFAULT_WIDTH = 400;

    /**
     * Main model.
     */
    private CompletionConsoleModel model;

    /**
     * Scene of the application.
     */
    private Scene scene;

    /**
     * User text area.
     */
    private TextArea area;

    /**
     * Proposal text field.
     */
    private TextField field;

    /**
     * Run the main application.
     *
     * @param args Arguments of the application
     */
    public static void main(final String[] args) {
        // Launch the GUI
        launch(args);
    }

    /**
     * Create the scene for the application.
     *
     * @return Created scene
     */
    private Scene createScene() {

        // Default size of the window
        final int width = DEFAULT_WIDTH;
        final int height = DEFAULT_HEIGHT;

        // Scene it
        return new Scene(new VBox(), width, height);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(final Stage primaryStage) throws Exception {

        // Set the main window title
        primaryStage.setTitle("Completion service console");

        // Scene it
        scene = createScene();

        // Widgets of the view
        final SplitPane mainPane = new SplitPane();
        mainPane.setOrientation(Orientation.VERTICAL);
        area = new TextArea();
        field = new TextField();
        mainPane.getItems().add(area);
        mainPane.getItems().add(field);

        // Connect the text area to the model
        model = new CompletionConsoleModel(field.textProperty());
        area.textProperty().addListener(model);

        // Add the widgets
        ((VBox) scene.getRoot()).getChildren().add(mainPane);

        // Show the editor
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() throws Exception {
        // Stop the application
        super.stop();
    }
}
