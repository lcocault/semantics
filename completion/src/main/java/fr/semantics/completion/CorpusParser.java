/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Parser of a corpus to prepare completion database.
 */
public class CorpusParser {

    /**
     * Delimiters of phrases.
     */
    private static final java.lang.String PHRASE_DELIMITERS = ".;:?!,";

    /**
     * Delimiters for words.
     */
    private static final java.lang.String WORD_DELIMITERS = " '";

    /**
     * Depth of the word tree.
     */
    private static final int DEPTH = 3;

    /**
     * Dictionary of words detected and corresponding occurrences count.
     */
    private Node root;

    /**
     * Logger.
     */
    private Logger logger = Logger.getLogger(CorpusParser.class.getName());

    /**
     * Constructor.
     */
    public CorpusParser() {
        root = new Node("");
    }

    /**
     * Clear the dictionary.
     */
    public void clear() {
        root.clear();
    }

    /**
     * Get the root node.
     *
     * @return Root node built by the parser
     */
    public Node getRoot() {
        return root;
    }

    /**
     * Parse the input stream to create a completion database.
     *
     * @param corpus Corpus to create the database
     */
    public void parse(final InputStream corpus) {

        // The file is read with a buffered reader
        final BufferedReader reader = new BufferedReader(
            new InputStreamReader(corpus));
        try {

            // Initialize the phrase
            StringBuilder phrase = new StringBuilder();

            // Read line after line
            String line = reader.readLine();
            while (line != null) {

                // Prepare phrase delimitation
                final StringTokenizer tokenizer = new StringTokenizer(line,
                    PHRASE_DELIMITERS, true);

                while (tokenizer.hasMoreTokens()) {
                    final String token = tokenizer.nextToken();
                    if (token.length() == 1 && PHRASE_DELIMITERS.contains(
                        token)) {
                        // End of the phrase
                        processPhrase(phrase.toString().trim());
                        // Next phrase
                        phrase = new StringBuilder();
                    } else {
                        // Complement the phrase
                        phrase.append(token);
                        phrase.append(" ");
                    }
                }


                // Next line
                line = reader.readLine();
            }
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }
    }

    /**
     * Process a phrase.
     *
     * @param phrase Phrase to process
     */
    private void processPhrase(final String phrase) {

        // Create a new tokenizer to separate words
        final StringTokenizer tokenizer = new StringTokenizer(phrase,
            WORD_DELIMITERS, false);

        // Words
        final String[] words = new String[DEPTH];

        while (tokenizer.hasMoreTokens()) {

            // Extract a word (lower case only)
            final String token = tokenizer.nextToken().toLowerCase();

            // Manage tokens: populate the table of words starting with the
            // token just read and shifting the existing words to the right.
            for (int index = 0; index < DEPTH - 1; index++) {
                words[index] = words[index + 1];
            }
            words[DEPTH - 1] = token;

            // Feed the root node when there is no more token to read or when
            // the words array is full.
            if (!tokenizer.hasMoreTokens() || words[0] != null) {
                // Add the path of nodes to the root node.
                Node node = root;
                for (int index = 0; index < DEPTH; index++) {
                    if (words[index] != null) {
                        node = node.addSubNode(words[index]);
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return root.toString();
    }
}
