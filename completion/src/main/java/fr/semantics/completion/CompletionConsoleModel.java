/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Model of the completion console.
 */
public class CompletionConsoleModel implements
    ChangeListener {

    /**
     * Corpus to load.
     */
    private static final String CORPUS = "corpus-science-fiction.txt";

    /** Logger. */
    private Logger logger = Logger.getLogger(CompletionConsoleModel.class
        .getName());

    /**
     * Proposed value.
     */
    private StringProperty proposed;

    /**
     * Completion service.
     */
    private CompletionService service;

    /**
     * Constructor.
     *
     * @param property Property to use as target value
     */
    public CompletionConsoleModel(final StringProperty property) {

        // Corpus
        Node corpus = null;

        // The corpus may be cached
        final File cache = new File("/tmp/corpus.data");
        if (cache.exists()) {
            try {
                // Try to load a cached corpus
                final FileInputStream fis = new FileInputStream(cache);
                final ObjectInputStream ois = new ObjectInputStream(fis);
                corpus = (Node) ois.readObject();
            } catch (IOException|ClassNotFoundException exception) {
                logger.warning("Cannot load the cached corpus");
                logger.warning(exception.getMessage());
            }
        }

        // If the corpus has not been retrieved from the cache, parse it
        if (corpus == null) {

            // Parse a corpus
            final CorpusParser parser = new CorpusParser();
            parser.parse(ClassLoader.getSystemResourceAsStream(CORPUS));
            corpus = parser.getRoot();

            try {
                // And create a cache
                final FileOutputStream fos = new FileOutputStream(cache);
                final ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(corpus);
            } catch (IOException exception) {
                logger.warning("Cannot create the cached corpus");
                logger.warning(exception.getMessage());
            }
        }

        // Set attributes
        this.proposed = property;
        this.service = new CompletionService(corpus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changed(final ObservableValue observable, final Object
        oldValue, final Object newVal) {

        // Words
        final String[] words = new String[2];
        String lastWord = "";

        // Tokenize the current user content
        final StringTokenizer tknzr = new StringTokenizer(newVal.toString());
        while (tknzr.hasMoreTokens()) {
            words[1] = words[0];
            words[0] = lastWord;
            lastWord = tknzr.nextToken();
        }

        // Compute the proposal
        proposed.setValue(service.resolve(words, lastWord));
    }

}
