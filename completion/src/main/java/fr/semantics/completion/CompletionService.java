/* Copyright 2017 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import java.util.Map;

/**
 * Completion service.
 */
public class CompletionService {

    /**
     * Dictionary of words and corresponding occurrences count.
     */
    private Node root;

    /**
     * Constructor.
     *
     * @param root Dictionnary to use
     */
    public CompletionService(final Node root) {
        this.root = root;
    }

    /**
     * Resolve a completion.
     *
     * @param path        List of words already written (may be an empty array)
     * @param onGoingWord Start if the word being written (may be an empty
     *                    string)
     * @return Proposed word, empty string if no word is candidate
     */
    public String resolve(final String[] path, final String onGoingWord) {

        // Only use lower case words
        final String prefix = onGoingWord.toLowerCase();

        // Get the appropriate root node
        Node node = root;
        for (int index = 0; index < path.length; index++) {
            if (path[index] != null) {
                final Node nextNode = node.searchNode(
                    path[index].toLowerCase());
                if (nextNode == null) {
                    // Stop iterating
                    break;
                } else {
                    // Continue iterating
                    node = nextNode;
                }
            }
        }

        // Search for the most probable word starting with the written
        // characters
        int higherScore = 0;
        String word = "";
        final Map<Node, Integer> subNodes = node.getSubNodes();
        for (Node candidate : subNodes.keySet()) {
            // Iterate through possible words. Only consider the ones that
            // start with the on going prefix. Then consider the score of
            // each candidate word.
            if (candidate.getWord().startsWith(prefix) && higherScore <
                subNodes.get(candidate)) {
                // Found a better candidate
                word = candidate.getWord();
                higherScore = subNodes.get(candidate);
            }
        }

        // If the word is not found, attempt to change the path with a
        // shorter one.
        if (word.isEmpty() && path[0] != null) {
            // Shift the path to shorten it
            for (int index = 0; index < path.length - 1; index++) {
                path[index] = path[index + 1];
            }
            path[path.length - 1] = null;
            // Recursively call
            word = resolve(path, onGoingWord);
        }

        // Return the word found
        return word;
    }
}
