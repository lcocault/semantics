/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

/**
 * A derived word that has a gender and a number.
 */
public class Derived extends Word {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /** Gender of the noun. */
    private Gender gender_;

    /** Number of the noun. */
    private Number number_;

    /**
     * Constructor.
     * @param identifier
     *            Identifier of the word
     * @param type
     *            Type of the word
     * @param text
     *            Text of the word
     * @param root
     *            Root of the word
     * @param gender
     *            Gender of the word
     * @param number
     *            Number of the word
     */
    public Derived(final int identifier, final Type type, final String text,
            final Word root, final Gender gender, final Number number) {
        super(identifier, type, text, root);
        gender_ = gender;
        number_ = number;
    }

    /**
     * Get the gender of the word.
     * @return Gender of the word
     */
    public Gender getGender() {
        return gender_;
    }

    /**
     * Get the number of the word.
     * @return Number of the word
     */
    public Number getNumber() {
        return number_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (gender_ != null && number_ != null) {
            return super.toString() + "," + gender_ + "," + number_;
        } else {
            return super.toString() + ",?,?";
        }
    }
}
