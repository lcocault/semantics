/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

/**
 * Any of a closed class of non-inflecting words typically employed to connect a
 * noun or a pronoun, in an adjectival or adverbial sense, with some other word:
 * a particle used with a noun or pronoun (in English always in the objective
 * case) to make a phrase limiting some other word.
 */
public class Preposition extends Word {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor of a preposition.
     * @param identifier
     *            Identifier of the word
     * @param text
     *            Text of the word
     */
    public Preposition(final int identifier, final String text) {
        super(identifier, Type.PREPOSITION, text);
    }

}
