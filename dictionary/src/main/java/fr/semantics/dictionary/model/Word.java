/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

import java.io.Serializable;

/**
 * Word of the natural language.
 */
public class Word implements Serializable {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /** Unique identifier of the word. */
    private int identifier_;

    /** Type of the word. */
    private Type type_;

    /** Semantic root. */
    private Word root_;

    /** Text of the word. */
    private String text_;

    /**
     * Constructor of a root word.
     * @param identifier
     *            Identifier of the word
     * @param type
     *            Type of the word
     * @param text
     *            Text of the word
     */
    public Word(final int identifier, final Type type, final String text) {
        identifier_ = identifier;
        type_ = type;
        text_ = text;
        root_ = null;
    }

    /**
     * Constructor of a derived word.
     * @param identifier
     *            Identifier of the word
     * @param type
     *            Type of the word
     * @param text
     *            Text of the word
     * @param root
     *            Root of the word
     */
    public Word(final int identifier, final Type type, final String text,
            final Word root) {
        identifier_ = identifier;
        type_ = type;
        text_ = text;
        root_ = root;
    }

    /**
     * Get the word identifier.
     * @return Identifier of the word
     */
    public int getIdentifier() {
        return identifier_;
    }

    /**
     * Get the root of the word.
     * @return Root of the word
     */
    public Word getRoot() {
        return root_;
    }

    /**
     * Get the text of the word.
     * @return Raw text of the word
     */
    public String getText() {
        return text_;
    }

    /**
     * Get the type of the word.
     * @return Type of the word
     */
    public Type getType() {
        return type_;
    }

    /**
     * Set the root of the word.
     * @param root
     *            Root of the word
     */
    public void setRoot(final Word root) {
        root_ = root;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        // Identifier of the root word (0 means no root)
        int rootId = 0;
        // Get the identifier of the root word
        if (root_ != null) {
            rootId = root_.getIdentifier();
        }
        // Return the word serialization
        return type_ + "," + identifier_ + "," + text_ + "," + rootId;
    }

}
