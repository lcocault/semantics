/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

/**
 * Type/kind of word.
 */
public enum Type {
    /** Adjective. @see https://en.wiktionary.org/wiki/adjective#Noun */
    ADJECTIVE,
    /** Adverb. @see https://en.wiktionary.org/wiki/adverb#Noun */
    ADVERB,
    /** Article. @see https://en.wiktionary.org/wiki/article#Noun */
    ARTICLE,
    /** Conjunction. @see https://en.wiktionary.org/wiki/conjunction#Noun */
    CONJUNCTION,
    /** Interjection. @see https://en.wiktionary.org/wiki/interjection#Noun */
    INTERJECTION,
    /** Interrogative pronoun. */
    INTERROGATIVE_PRONOUN,
    /** Noun. @see https://en.wiktionary.org/wiki/noun#Noun */
    NOUN,
    /** Onomatopoeia. @see https://en.wiktionary.org/wiki/onomatopoeia#Noun */
    ONOMATOPOEIA,
    /** Preposition. @see https://en.wiktionary.org/wiki/preposition#Noun */
    PREPOSITION,
    /** Pronoun. @see https://en.wiktionary.org/wiki/pronoun#Noun */
    PRONOUN,
    /** Verb. @see https://en.wiktionary.org/wiki/verb#Noun */
    VERB
}
