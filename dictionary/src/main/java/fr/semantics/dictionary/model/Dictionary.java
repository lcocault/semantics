/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Full dictionary.
 */
public class Dictionary implements Serializable {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /** Maximum allocated identifier. */
    private int maxIdentifier_;

    /** Index of the words. */
    private Map<Integer, Word> words_;

    /** Map of words for textual searches. */
    private Map<String, Map<Type, Set<Word>>> wordsByText_;

    /**
     * Constructor.
     */
    public Dictionary() {
        maxIdentifier_ = 0;
        words_ = new HashMap<Integer, Word>();
        wordsByText_ = new HashMap<String, Map<Type, Set<Word>>>();
    }

    /**
     * Add a new word to the dictionary.
     * @param word
     *            Word to add
     */
    public void addWord(final Word word) {

        // Update the maximum identifier
        final int identifier = word.getIdentifier();
        if (identifier > maxIdentifier_) {
            maxIdentifier_ = identifier;
        }

        // Classification by identifier
        words_.put(identifier, word);

        // Classification by text
        Map<Type, Set<Word>> map = wordsByText_.get(word.getText());
        if (map == null) {
            map = new HashMap<Type, Set<Word>>();
            wordsByText_.put(word.getText(), map);
        }
        Set<Word> words = map.get(word.getType());
        if (words == null) {
            words = new HashSet<Word>();
            map.put(word.getType(), words);
        }
        words.add(word);
    }

    /**
     * Get a word by its identifier.
     * @param identifier
     *            Identifier of the expected word
     * @return Word corresponding to the identifier
     */
    public Word getWord(final int identifier) {
        return words_.get(identifier);
    }

    /**
     * Get words by a textual content.
     * @param rootText
     *            Text of the searched words
     * @param type
     *            Type of searched words
     * @return List of matching words, null if there is no matching word
     */
    public Set<Word> getWord(final String rootText, final Type type) {

        // Result
        Set<Word> words = null;

        // Get the expected set
        final Map<Type, Set<Word>> map = wordsByText_.get(rootText);
        if (map == null) {
            // Never return a null result
            words = new HashSet<Word>();
        } else {
            words = map.get(type);
        }

        return words;
    }

    /**
     * Get the list of all the words.
     * @return All the words in the dictionary
     */
    public Collection<Word> getWords() {
        return words_.values();
    }

    /**
     * Get the words for a given token.
     * @param token
     *            Token to match
     * @return Set of possible words
     */
    public Map<Type, Set<Word>> getWords(final String token) {

        // Get the words
        Map<Type, Set<Word>> words = wordsByText_.get(token);

        // If empty, return an empty collection (not null)
        if (words == null) {
            words = new HashMap<Type, Set<Word>>();
        }

        return words;
    }

}
