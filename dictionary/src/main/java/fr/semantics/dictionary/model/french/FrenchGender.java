/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model.french;

import fr.semantics.dictionary.model.Gender;

/**
 * French genders.
 */
public enum FrenchGender implements Gender {

    /** Masculine gender. */
    MASCULIN,
    /** Feminine gender. */
    FEMININ,
    /** For words that have no gender. */
    INVARIABLE;

    /**
     * Get the value of the gender from its identifier.
     * @param identifier
     *            Identifier of the value
     * @return Gender value
     */
    public static FrenchGender create(final String identifier) {
        return FrenchGender.valueOf(identifier);
    }

}
