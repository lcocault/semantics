/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

/**
 * An exclamation or filled pause; a word or phrase with no particular
 * grammatical relation to a sentence, often an expression of emotion.
 */
public class Interjection extends Word {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor of an interjection.
     * @param identifier
     *            Identifier of the word
     * @param text
     *            Text of the word
     */
    public Interjection(final int identifier, final String text) {
        super(identifier, Type.INTERJECTION, text);
    }

}
