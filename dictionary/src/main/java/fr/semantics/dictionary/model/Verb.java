/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

/**
 * A word that indicates an action, event, or state.
 */
public class Verb extends Derived {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /** Tense of the verb. */
    private Tense tense_;

    /**
     * Constructor.
     * @param identifier
     *            Identifier of the word
     * @param text
     *            Text of the word
     * @param root
     *            Root of the word
     * @param gender
     *            Gender of the word
     * @param number
     *            Number of the word
     * @param tense
     *            Tense of the verb
     */
    public Verb(final int identifier, final String text, final Word root,
            final Gender gender, final Number number, final Tense tense) {
        super(identifier, Type.VERB, text, root, gender, number);
        tense_ = tense;
    }

    /**
     * Get the tense of the verb.
     * @return Tense of the verb
     */
    public Tense getTense() {
        return tense_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return super.toString() + "," + tense_;
    }

}
