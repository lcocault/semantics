/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

/**
 * A type of noun that refers anaphorically to another noun or noun phrase, but
 * which cannot ordinarily be preceded by a determiner and rarely takes an
 * attributive adjective. English examples include I, you, him, who, me, my,
 * each other.
 */
public class Pronoun extends Derived {

    /** Version of the class. */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * @param identifier
     *            Identifier of the word
     * @param text
     *            Text of the word
     * @param root
     *            Root of the word
     * @param gender
     *            Gender of the word
     * @param number
     *            Number of the word
     */
    public Pronoun(final int identifier, final String text, final Word root,
            final Gender gender, final Number number) {
        super(identifier, Type.PRONOUN, text, root, gender, number);
    }

    /**
     * Constructor.
     * @param identifier
     *            Identifier of the word
     * @param type
     *            Type of the pronoun
     * @param text
     *            Text of the word
     * @param root
     *            Root of the word
     * @param gender
     *            Gender of the word
     * @param number
     *            Number of the word
     */
    protected Pronoun(final int identifier, final Type type, final String text,
            final Word root, final Gender gender, final Number number) {
        super(identifier, type, text, root, gender, number);
    }

}
