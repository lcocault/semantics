/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import fr.semantics.dictionary.model.Dictionary;
import fr.semantics.dictionary.model.Word;

public class DictionaryWriter {

    /**
     * Write a dictionary to a file.
     * @param dictionary
     *            Dictionary to write
     * @param file
     *            File in which the dictionary should be written
     * @throws IOException
     *             Cannot write the output file
     */
    public void write(final Dictionary dictionary, final File file)
        throws IOException {

        // Output stream
        final FileOutputStream os = new FileOutputStream(file);
        final PrintWriter pw = new PrintWriter(os);

        try {
            // Write the words of the dictionary
            for (Word word : dictionary.getWords()) {
                pw.println(word.toString());
            }

        } finally {

            // Finalize the file
            pw.close();
        }
    }

}
