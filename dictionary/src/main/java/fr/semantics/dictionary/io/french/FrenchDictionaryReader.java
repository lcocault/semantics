/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.io.french;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import fr.semantics.dictionary.io.DictionaryReader;
import fr.semantics.dictionary.model.Adjective;
import fr.semantics.dictionary.model.Adverb;
import fr.semantics.dictionary.model.Article;
import fr.semantics.dictionary.model.Conjunction;
import fr.semantics.dictionary.model.Dictionary;
import fr.semantics.dictionary.model.Gender;
import fr.semantics.dictionary.model.Interjection;
import fr.semantics.dictionary.model.InterrogativePronoun;
import fr.semantics.dictionary.model.Noun;
import fr.semantics.dictionary.model.Number;
import fr.semantics.dictionary.model.Onomatopoeia;
import fr.semantics.dictionary.model.Preposition;
import fr.semantics.dictionary.model.Pronoun;
import fr.semantics.dictionary.model.Tense;
import fr.semantics.dictionary.model.Type;
import fr.semantics.dictionary.model.Verb;
import fr.semantics.dictionary.model.Word;
import fr.semantics.dictionary.model.french.FrenchGender;
import fr.semantics.dictionary.model.french.FrenchNumber;
import fr.semantics.dictionary.model.french.FrenchTense;

/**
 * Reader for a dictionary file.
 */
public class FrenchDictionaryReader implements DictionaryReader {

    /**
     * {@inheritDoc}
     */
    @Override
    public Dictionary read(final File file) throws IOException {

        // Dictionary
        final Dictionary dictionary = new Dictionary();
        final Map<Word, Integer> parents = new HashMap<Word, Integer>();

        // Extract the words
        extractWords(file, dictionary, parents);

        // Link the words
        final Iterator<Word> words = parents.keySet().iterator();
        while (words.hasNext()) {

            // Word to link
            final Word word = words.next();
            final int rootId = parents.get(word);

            // Set the root
            word.setRoot(dictionary.getWord(rootId));
        }

        return dictionary;
    }

    /**
     * Perform the initial parsing of the file to extract the words.
     * @param file
     *            Path of the file to read
     * @param dictionary
     *            Dictionary to store the words
     * @param parents
     *            Map of word parents
     * @throws IOException
     *             Cannot read the file
     */
    private void extractWords(final File file, final Dictionary dictionary,
            final Map<Word, Integer> parents) throws IOException {

        // Read file line per line
        final FileReader fileReader = new FileReader(file);
        final BufferedReader reader = new BufferedReader(fileReader);

        // Go read !
        String line = reader.readLine();
        while (line != null) {

            // Word to create
            Word word = null;

            // Word attributes
            int identifier = 0;
            int rootIdentifier = 0;
            Type type = null;
            Gender gender = null;
            Number number = null;
            Tense tense = null;

            // Each line must tokenized with commas
            final StringTokenizer tokenizer = new StringTokenizer(line, ",");
            final String readType = tokenizer.nextToken();
            final String readIdentifier = tokenizer.nextToken();
            final String readText = tokenizer.nextToken();
            final String readRoot = tokenizer.nextToken();

            // Optional fields
            if (tokenizer.hasMoreTokens()) {
                // Gender and number
                final String readGender = tokenizer.nextToken();
                final String readNumber = tokenizer.nextToken();
                if (readGender.length() > 1) {
                    gender = FrenchGender.create(readGender);
                }
                if (readNumber.length() > 1) {
                    number = FrenchNumber.create(readNumber);
                }
            }
            if (tokenizer.hasMoreTokens()) {
                // Tense
                final String readTense = tokenizer.nextToken();
                tense = FrenchTense.create(readTense);
            }

            // Compute the type and identifier
            type = Type.valueOf(readType);
            identifier = Integer.parseInt(readIdentifier);
            rootIdentifier = Integer.parseInt(readRoot);

            // The rest of the parsing depends on the type
            switch (type) {
            case ADJECTIVE:
                // Adjective
                word = new Adjective(identifier, readText, null, gender, number);
                break;
            case ADVERB:
                // Adverb
                word = new Adverb(identifier, readText);
                break;
            case ARTICLE:
                // Article
                word = new Article(identifier, readText, null, gender, number);
                break;
            case CONJUNCTION:
                // Conjunction
                word = new Conjunction(identifier, readText);
                break;
            case INTERJECTION:
                // Interjection
                word = new Interjection(identifier, readText);
                break;
            case INTERROGATIVE_PRONOUN:
                // Interrogative pronoun
                word = new InterrogativePronoun(identifier, readText, null,
                        gender, number);
                break;
            case NOUN:
                // Noun
                word = new Noun(identifier, readText, null, gender, number);
                break;
            case ONOMATOPOEIA:
                // Onomatopoeia
                word = new Onomatopoeia(identifier, readText);
                break;
            case PREPOSITION:
                // Preposition
                word = new Preposition(identifier, readText);
                break;
            case PRONOUN:
                // Pronoun
                word = new Pronoun(identifier, readText, null, gender, number);
                break;
            case VERB:
                // Verb
                word = new Verb(identifier, readText, null, gender, number,
                        tense);
                break;
            default:
                // Unsupported kind of word for a French dictionary
            }

            // Add the word to the dictionary and keep the track of the root
            // text
            if (word != null) {
                dictionary.addWord(word);
                parents.put(word, rootIdentifier);
            }

            // Next line
            line = reader.readLine();
        }

        // Close the reader
        reader.close();
    }

}
