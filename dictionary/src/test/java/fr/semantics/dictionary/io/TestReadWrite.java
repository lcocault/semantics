/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

import fr.semantics.dictionary.io.french.FrenchDictionaryReader;
import fr.semantics.dictionary.model.Dictionary;

public class TestReadWrite {

    @Test
    public void testBasic() throws IOException {

        // Files
        File input = new File("src/main/resources/french.csv");
        File output = new File("src/test/resources/french-copy.csv");

        // Read the French dictionary
        DictionaryReader reader = new FrenchDictionaryReader();
        Dictionary dictionary = reader.read(input);

        // Write it
        DictionaryWriter writer = new DictionaryWriter();
        writer.write(dictionary, output);

        // Compare files are check they are identical
        FileReader inputReader = new FileReader(input);
        FileReader outputReader = new FileReader(output);
        int inputByte = inputReader.read();
        int outputByte = outputReader.read();
        // Comparison is done byte after byte
        while (inputByte != -1) {
            assertEquals(inputByte, outputByte);
            inputByte = inputReader.read();
            outputByte = outputReader.read();
        }
        // When the input has been read totally, the output must also be fully
        // read
        assertEquals(outputByte, -1);
        // Close the reads
        inputReader.close();
        outputReader.close();
        // Delete the output file
        assertTrue(output.delete());
    }

}
