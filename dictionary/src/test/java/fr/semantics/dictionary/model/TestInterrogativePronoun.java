/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.semantics.dictionary.model.french.FrenchGender;
import fr.semantics.dictionary.model.french.FrenchNumber;

public class TestInterrogativePronoun {

    @Test
    public void testBasic() {

        // Constructors
        InterrogativePronoun root = new InterrogativePronoun(123, "quel", null,
                FrenchGender.MASCULIN, FrenchNumber.SINGULIER);
        InterrogativePronoun derived = new InterrogativePronoun(456, "quelles",
                root, FrenchGender.FEMININ, FrenchNumber.PLURIEL);

        // Getters
        assertEquals(123, root.getIdentifier());
        assertEquals(Type.INTERROGATIVE_PRONOUN, root.getType());
        assertEquals("quel", root.getText());
        assertEquals(null, root.getRoot());
        assertEquals(FrenchGender.MASCULIN, root.getGender());
        assertEquals(FrenchNumber.SINGULIER, root.getNumber());
        assertEquals(456, derived.getIdentifier());
        assertEquals(Type.INTERROGATIVE_PRONOUN, derived.getType());
        assertEquals("quelles", derived.getText());
        assertEquals(root, derived.getRoot());
        assertEquals(FrenchGender.FEMININ, derived.getGender());
        assertEquals(FrenchNumber.PLURIEL, derived.getNumber());
    }

}
