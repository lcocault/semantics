/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.semantics.dictionary.model.french.FrenchGender;
import fr.semantics.dictionary.model.french.FrenchNumber;
import fr.semantics.dictionary.model.french.FrenchTense;

public class TestVerb {

    @Test
    public void testBasic() {

        // Constructors
        Verb root = new Verb(123, "coder", null, FrenchGender.INVARIABLE,
                FrenchNumber.SINGULIER, FrenchTense.INFINITIF);
        Verb derived = new Verb(456, "codent", root, FrenchGender.INVARIABLE,
                FrenchNumber.PLURIEL, FrenchTense.INDICATIF_PRESENT);

        // Getters
        assertEquals(123, root.getIdentifier());
        assertEquals(Type.VERB, root.getType());
        assertEquals("coder", root.getText());
        assertEquals(null, root.getRoot());
        assertEquals(FrenchGender.INVARIABLE, root.getGender());
        assertEquals(FrenchNumber.SINGULIER, root.getNumber());
        assertEquals(FrenchTense.INFINITIF, root.getTense());
        assertEquals("VERB,123,coder,0,INVARIABLE,SINGULIER,INFINITIF",
                root.toString());
        assertEquals(456, derived.getIdentifier());
        assertEquals(Type.VERB, derived.getType());
        assertEquals("codent", derived.getText());
        assertEquals(root, derived.getRoot());
        assertEquals(FrenchGender.INVARIABLE, derived.getGender());
        assertEquals(FrenchNumber.PLURIEL, derived.getNumber());
        assertEquals(FrenchTense.INDICATIF_PRESENT, derived.getTense());
        assertEquals(
                "VERB,456,codent,123,INVARIABLE,PLURIEL,INDICATIF_PRESENT",
                derived.toString());
    }

}
