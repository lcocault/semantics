/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestWord {

    @Test
    public void testBasic() {

        // Constructors
        Word root = new Word(123, Type.ADJECTIVE, "heureux");
        Word derived = new Word(456, Type.ADJECTIVE, "heureuse", root);

        // Getters
        assertEquals(123, root.getIdentifier());
        assertEquals(Type.ADJECTIVE, root.getType());
        assertEquals("heureux", root.getText());
        assertEquals(null, root.getRoot());
        assertEquals("ADJECTIVE,123,heureux,0", root.toString());
        assertEquals(456, derived.getIdentifier());
        assertEquals(Type.ADJECTIVE, derived.getType());
        assertEquals("heureuse", derived.getText());
        assertEquals(root, derived.getRoot());
        assertEquals("ADJECTIVE,456,heureuse,123", derived.toString());

        // Setters
        derived.setRoot(null);
        assertEquals(null, derived.getRoot());
    }

}
