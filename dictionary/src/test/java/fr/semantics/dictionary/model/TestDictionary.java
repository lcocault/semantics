/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.dictionary.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestDictionary {

    @Test
    public void testBasic() {

        // Mixture
        Word root = new Word(123, Type.ADJECTIVE, "français");
        Word derived = new Word(456, Type.ADJECTIVE, "française", root);
        Word noun = new Word(789, Type.NOUN, "français");

        // Constructor
        Dictionary dictionary = new Dictionary();

        // Initially empty
        assertEquals(0, dictionary.getWords("français").size());
        assertEquals(0, dictionary.getWord("français", Type.ADJECTIVE).size());

        // Add the words
        dictionary.addWord(root);
        dictionary.addWord(derived);
        dictionary.addWord(noun);

        // Get the words
        assertEquals(2, dictionary.getWords("français").size());
        assertEquals(1, dictionary.getWords("française").size());
        assertEquals(1, dictionary.getWord("français", Type.ADJECTIVE).size());
        assertEquals(noun, dictionary.getWord(789));
    }

}
