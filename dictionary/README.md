# Scope

The Java based structured dictionary is a set of concept that describes a
natural language dictionary. The default instance is a French dictionary but
the concepts apply to many other natural languages. 

#License

It is licensed by Laurent COCAULT under the Apache License Version 2.0. A copy
of this license is provided in the LICENSE.txt file.

# Content

The src/main/java directory contains the library sources.
The src/main/resources directory contains the library data.
The src/test/java directory contains the tests sources.
The src/test/resources directory contains the tests data.

# Dependencies

Java based dictionary relies on the following free software, all released under
business friendly free licenses.

test-time dependency:

  - JUnit 4.12 from Erich Gamma and Kent Beck
    http://www.junit.org/
    released under the Common Public License Version 1.0
