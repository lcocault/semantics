/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;

import org.junit.Test;

import fr.semantics.statistics.TextParser;

public class TestTextParser {

    @Test
    public void testBasic() {

        // Fixture
        String text = "Hello Helen! He is \nhere. He hears me, if \nhe " +
                "is\n  near.";
        ByteArrayInputStream bis = new ByteArrayInputStream(text.getBytes());

        // Tested object
        TextParser parser = new TextParser();
        parser.parse(bis);
        assertEquals(3, parser.getCount("he"));
        assertEquals(1, parser.getCount("hears"));
        assertEquals(0, parser.getCount("nope"));
    }

    @Test
    public void testDialogExtraction() {

        // Fixture
        String text = "\"Hello Helen! He is \nhere.\" said John.\nAfter a " +
                "while, he continued.\n- He hears me, \n  if he is near.\n" +
                "- You are right!\nThen he left.";
        ByteArrayInputStream bis = new ByteArrayInputStream(text.getBytes());

        // Tested object
        TextParser parser = new TextParser();
        parser.parse(bis);
        assertEquals(5, parser.getCount("he"));
        assertEquals(3, parser.getDialogCount("he"));
        assertEquals(1, parser.getDialogCount("if"));
        assertEquals(0, parser.getDialogCount("nope"));
    }

}
