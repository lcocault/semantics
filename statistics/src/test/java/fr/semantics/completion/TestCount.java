/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.semantics.statistics.Count;

public class TestCount {

    @Test
    public void testDefault() {
        Count count = new Count("Test");
        assertEquals(1, count.getCount());
        assertEquals("Test", count.getWord());
    }

    @Test
    public void testIncrement() {
        Count count = new Count("Test");
        count.increment();
        assertEquals(2, count.getCount());
    }

    @Test
    public void testCompare() {
        Count count1 = new Count("Test1");
        Count count2 = new Count("Test1");
        Count count3 = new Count("Test2");
        count1.increment();
        assertTrue(count1.compareTo(count2) < 0);
        assertTrue(count2.compareTo(count3) < 0);
    }

}
