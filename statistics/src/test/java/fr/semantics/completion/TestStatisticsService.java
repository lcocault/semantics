/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.completion;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fr.semantics.statistics.Count;
import fr.semantics.statistics.StatisticsService;

public class TestStatisticsService {

    @Test
    public void testBasic() {
        // GIVEN a new statistics service
        StatisticsService service = new StatisticsService();

        // WHEN two identical words and another word are added
        service.addWord("One");
        service.addWord("Two");
        service.addWord("One");

        // THEN the word One is counted twice, the word Two once and the word
        // Three zero
        assertEquals(2, service.getCount("One"));
        assertEquals(1, service.getCount("Two"));
        assertEquals(0, service.getCount("Three"));
    }

    @Test
    public void testBasicDialog() {
        // GIVEN a new statistics service
        StatisticsService service = new StatisticsService();

        // WHEN two identical dialog words and another word are added
        service.addDialogWord("One");
        service.addDialogWord("Two");
        service.addDialogWord("One");

        // THEN the word One is counted twice, the word Two once and the word
        // Three zero
        assertEquals(2, service.getDialogCount("One"));
        assertEquals(1, service.getDialogCount("Two"));
        assertEquals(0, service.getDialogCount("Three"));
    }

    @Test
    public void testMostFrequentAll() {
        // GIVEN a new statistics service
        StatisticsService service = new StatisticsService();

        // WHEN One is added 5 times, Two and Three three times each and Four
        // just once
        for (int i = 0; i < 5; i++) {
            service.addWord("One");
        }
        for (int i = 0; i < 3; i++) {
            service.addWord("Two");
        }
        for (int i = 0; i < 3; i++) {
            service.addWord("Three");
        }
        service.addWord("Four");

        // THEN the word are ranked in the order: One, Three, Two, Four
        List<Count> counts = service.getMostFrequentWords(5);
        assertEquals(4, counts.size());
        assertEquals("One", counts.get(0).getWord());
        assertEquals("Three", counts.get(1).getWord());
        assertEquals("Two", counts.get(2).getWord());
        assertEquals("Four", counts.get(3).getWord());
    }

    @Test
    public void testMostFrequentSubset() {
        // GIVEN a new statistics service
        StatisticsService service = new StatisticsService();

        // WHEN One is added 5 times, Two and Three three times each and Four
        // just once
        for (int i = 0; i < 5; i++) {
            service.addWord("One");
        }
        for (int i = 0; i < 3; i++) {
            service.addWord("Two");
        }
        for (int i = 0; i < 3; i++) {
            service.addWord("Three");
        }
        service.addWord("Four");

        // THEN the word are ranked in the order: One, Three
        List<Count> counts = service.getMostFrequentWords(2);
        assertEquals(2, counts.size());
        assertEquals("One", counts.get(0).getWord());
        assertEquals("Three", counts.get(1).getWord());
    }

}
