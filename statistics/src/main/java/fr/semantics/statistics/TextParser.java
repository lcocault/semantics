/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.statistics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Parser of a text to prepare statistics database.
 */
public class TextParser {

    /** Delimiters of phrases. */
    private static final java.lang.String PHRASE_DELIMITERS = ".;:?!";

    /** Delimiters for words. */
    private static final java.lang.String WORD_DELIMITERS = " '";

    /** Delimiter for dialog sections. */
    private static final String DIALOG_QUOTE = "\"";

    /** Prefix for a dialog phrase. */
    private static final String DIALOG_HYPHEN = "-";

    /** Statistics service. */
    private StatisticsService service;

    /**
     * Logger.
     */
    private Logger logger = Logger.getLogger(TextParser.class.getName());

    /**
     * Constructor.
     */
    public TextParser() {
        service = new StatisticsService();
    }

    /**
     * Get the number of occurrences of the given word in the whole text.
     * @param word
     *            Word to count
     * @return Number of occurrences
     */
    public int getCount(String word) {
        return service.getCount(word);
    }

    /**
     * Get the number of occurrences of the given word in a dialog section.
     * @param word
     *            Word to count
     * @return Number of occurrences
     */
    public int getDialogCount(String word) {
        return service.getDialogCount(word);
    }

    /**
     * Get the most frequent words of the text.
     * @param limit
     *            Maximum number of word to return
     * @return List of word counts
     */
    public List<Count> getMostFrequentWords(int limit) {
        return service.getMostFrequentWords(limit);
    }

    /**
     * Parse the input stream to complement a statistics database.
     * @param corpus
     *            Corpus to create the database
     */
    public void parse(final InputStream corpus) {

        // The file is read with a buffered reader
        final BufferedReader reader = new BufferedReader(
                new InputStreamReader(corpus));
        try {

            // Initialize the phrase
            StringBuilder phrase = new StringBuilder();
            boolean dialogSection = false;

            // Read line after line
            String line = reader.readLine();
            while (line != null) {

                // Prepare phrase delimitation
                final StringTokenizer tokenizer = new StringTokenizer(line,
                        PHRASE_DELIMITERS, true);

                while (tokenizer.hasMoreTokens()) {
                    final String token = tokenizer.nextToken();
                    if (token.length() == 1 &&
                            PHRASE_DELIMITERS.contains(token)) {
                        // End of the phrase
                        String phraseToProcess = phrase.toString().trim();
                        logger.fine("Processing phrase: " + phraseToProcess);
                        if (phraseToProcess.startsWith(DIALOG_QUOTE)) {
                            dialogSection = !dialogSection;
                        }
                        boolean dialog = dialogSection ||
                                phraseToProcess.startsWith(DIALOG_HYPHEN);
                        processPhrase(phraseToProcess, dialog);
                        // Next phrase
                        phrase = new StringBuilder();
                    } else {
                        // Complement the phrase
                        phrase.append(token);
                        phrase.append(" ");
                    }
                }

                // Next line
                line = reader.readLine();
            }
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }
    }

    /**
     * Process a phrase.
     *
     * @param phrase
     *            Phrase to process
     * @param dialog
     *            Is the phrase in a dialog section?
     */
    private void processPhrase(final String phrase, final boolean dialog) {

        // Create a new tokenizer to separate words
        final StringTokenizer tokenizer = new StringTokenizer(phrase,
                WORD_DELIMITERS, false);

        while (tokenizer.hasMoreTokens()) {

            // Extract a word (lower case only)
            final String token = tokenizer.nextToken().toLowerCase();
            service.addWord(token);
            if (dialog) {
                service.addDialogWord(token);
            }
        }
    }

    /**
     * Set the qualifier associated with the statistics service.
     * @param qualifier
     *            Qualifier for the words of the statistics service
     */
    public void setWordQualifier(WordQualifier qualifier) {
        service.setQualifier(qualifier);
    }

}
