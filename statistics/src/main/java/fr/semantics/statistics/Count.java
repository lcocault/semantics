/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.statistics;

/**
 * References count.
 */
public class Count implements Comparable<Count> {

    /** Counted word. */
    private String word;

    /** Number of occurrences. */
    private int count;

    /**
     * Constructor of the reference.
     */
    public Count(String word) {
        this.word = word;
        this.count = 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(Count other) {
        if (count == other.count) {
            return word.compareTo(other.word);
        } else {
            return other.count - count;
        }
    }

    /**
     * Get the number of occurrences of the word.
     * @return Occurrences count
     */
    public int getCount() {
        return count;
    }

    /**
     * Get the counted word.
     * @return Word associated with the count
     */
    public String getWord() {
        return word;
    }

    /**
     * Increment the number of references of the word.
     */
    public void increment() {
        count++;
    }

}
