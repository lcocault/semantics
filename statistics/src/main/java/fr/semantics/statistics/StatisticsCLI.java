/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.statistics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import fr.semantics.dictionary.io.french.FrenchDictionaryReader;
import fr.semantics.dictionary.model.Dictionary;

/**
 * Command line interface to parse texts.
 */
public class StatisticsCLI {

    /** Dictionary to use. */
    private static final File DICTIONARY = new File(
            "../dictionary/src/main/resources/french.csv");

    /**
     * Command line interface entry point.
     * @param args
     *            Arguments of the program
     */
    public static void main(String args[]) {

        // One argument is mandatory
        if (args.length == 0) {
            System.err.println("At least one argument is expected");
            System.exit(-1);
        }

        // File to parse
        TextParser parser = new TextParser();
        File file = new File(args[0]);

        // Parse nouns only
        FrenchDictionaryReader reader = new FrenchDictionaryReader();
        Dictionary dictionary = null;
        try {
            dictionary = reader.read(DICTIONARY);
        } catch (IOException e) {
            System.err.println("The dictionary is not valid");
        }
        parser.setWordQualifier(new NounQualifier(dictionary));

        try {
            if (file.isDirectory()) {
                // Parse files in the directory
                for (File includedFile : file.listFiles()) {
                    parser.parse(new FileInputStream(includedFile));
                }
            } else {
                // Parse the file
                parser.parse(new FileInputStream(file));
            }
        } catch (FileNotFoundException e) {
            System.err.println("The file " + args[0] + " does not exist");
        }

        // Display the top 10 of most frequent words
        List<Count> counts = parser.getMostFrequentWords(20);
        for (Count count : counts) {
            System.out.println(count.getWord() + " -> " + count.getCount());
        }
    }

}
