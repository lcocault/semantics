/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Statistics service.
 */
public class StatisticsService {

    /** Count of words. */
    private Map<String, Count> words;

    /** Count of words in dialog sections. */
    private Map<String, Count> dialogWords;

    /** Word qualifier. */
    private WordQualifier qualifier;

    /**
     * Constructor of the service.
     */
    public StatisticsService() {
        words = new TreeMap<>();
        dialogWords = new TreeMap<>();
        qualifier = new NeutralQualifier();
    }

    /**
     * Add a word to compute statistics.
     * @param word
     *            New word to update statistics
     */
    public void addWord(final String word) {
        final String qualified = qualifier.qualify(word);
        if (words.containsKey(qualified)) {
            words.get(qualified).increment();
        } else {
            words.put(qualified, new Count(qualified));
        }
    }

    /**
     * Add a word from a dialog section to compute statistics.
     * @param word
     *            New word to update statistics
     */
    public void addDialogWord(final String word) {
        final String qualified = qualifier.qualify(word);
        if (dialogWords.containsKey(qualified)) {
            dialogWords.get(qualified).increment();
        } else {
            dialogWords.put(qualified, new Count(qualified));
        }
    }

    /**
     * Get the number of occurrences of the given word in the whole text.
     * @param word
     *            Word to count
     * @return Number of occurrences
     */
    public int getCount(String word) {
        if (words.containsKey(word)) {
            return words.get(word).getCount();
        } else {
            return 0;
        }
    }

    /**
     * Get the number of occurrences of the given word in a dialog section.
     * @param word
     *            Word to count
     * @return Number of occurrences
     */
    public int getDialogCount(String word) {
        if (dialogWords.containsKey(word)) {
            return dialogWords.get(word).getCount();
        } else {
            return 0;
        }
    }

    /**
     * Get the most frequent words of the text.
     * @param limit
     *            Maximum number of word to return
     * @return List of word counts
     */
    public List<Count> getMostFrequentWords(int limit) {
        // Create a copy of the list of words and sort them in the order from
        // the most frequent to the less frequent word
        List<Count> all = new ArrayList<>(words.values());
        Collections.sort(all);
        // Truncate the result when relevant
        if (limit > all.size()) {
            return all;
        } else {
            return all.subList(0, limit);
        }
    }

    /**
     * Change the word qualifier.
     * @param wordQualifier
     *            New word qualifier
     */
    public void setQualifier(WordQualifier wordQualifier) {
        qualifier = wordQualifier;
    }

}
