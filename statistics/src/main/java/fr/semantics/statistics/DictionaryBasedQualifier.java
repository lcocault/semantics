/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.semantics.statistics;

import java.util.Map;
import java.util.Set;

import fr.semantics.dictionary.model.Dictionary;
import fr.semantics.dictionary.model.Type;
import fr.semantics.dictionary.model.Word;

/**
 * Implementation of the word qualifier based on a dictionary.
 */
public class DictionaryBasedQualifier implements WordQualifier {

    /** Dictionary to qualify the word. */
    protected Dictionary dictionary;

    /**
     * Constructor.
     * @param dictionary
     *            Dictionary used to qualify the words
     */
    public DictionaryBasedQualifier(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String qualify(String word) {
        Map<Type, Set<Word>> candidates = dictionary.getWords(word);
        if (candidates.isEmpty()) {
            // When the word is not in the dictionary, it is ignored
            return "";
        } else {
            // Otherwise, it is returned as is
            return word;
        }
    }
}
